#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int cis[8];
pthread_mutex_t mutex[8];
pthread_mutex_t mutex_out;

void *fce(void *n)
{
    int num = *(int *)n, tmp;
    for (int i = 0; i < 10000; i++) {
        pthread_mutex_lock(&mutex[(num + i) % 8]);
        pthread_mutex_lock(&mutex[(num + i + 1) % 8]);

        tmp = cis[(num + i + 1) % 8];
        cis[(num + i + 1) % 8] = cis[(num + i) % 8];
        cis[(num + i) % 8] = tmp;

        pthread_mutex_unlock(&mutex[(num + i + 1) % 8]);
        pthread_mutex_unlock(&mutex[(num + i) % 8]);
        if ((i % 10) == 0) {
            pthread_mutex_lock(&mutex_out);
            printf("Thread %i i %i\n", num, i);
            pthread_mutex_unlock(&mutex_out);
        }
    }
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t tid[8];
    int n[8];
    pthread_mutex_init(&mutex_out, NULL);
    for (int i = 0; i < 8; i++) {
        pthread_mutex_init(&mutex[i], NULL);
        cis[i] = i;
        n[i] = i;
    }

    for (int i = 0; i < 8; i++) {
        pthread_create(&tid[i], NULL, fce, n + i);
    }
    for (int i = 0; i < 8; i++) {
        pthread_join(tid[i], NULL);
    }
    for (int i = 0; i < 8; i++) {
        printf("Data %i jsou %i\n", i, cis[i]);
    }
    return 0;
}
