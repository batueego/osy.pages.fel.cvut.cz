#include <stdio.h>
#include <unistd.h>

int b()
{
    printf("Co tu delam?\n");
    _exit(1);
}

int main();

void f(int x)
{
    unsigned long local[2], sp;
    int i;

    local[0] = 1;
    local[1] = 2;
    asm volatile("mov  %%rsp, %%rax; " : "=a"(sp) : :);
    for (i = 9; i >= 0; i--) {
        printf("%i - %016lx\n", i, local[i]);
    }
    printf("main %p\n", &main);
    printf("rsp %016lx\n", sp);
    local[5] = (unsigned long)&b;
}

int main()
{
    f(10);
    printf("Proc?\n");
    return 0;
}
