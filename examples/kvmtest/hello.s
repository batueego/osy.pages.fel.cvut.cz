	.code16
	.global start
start:
	mov $0x3f8, %dx
	mov $msg, %si
next:
	mov (%si), %al
	or  %al,%al
	jz  halt
	out %al, (%dx)
	inc %si
	jmp next
halt:
	hlt

msg:	.asciz "Hello world!\n"
