/* Sample code for /dev/kvm API
 *
 * Copyright (c) 2015 Intel Corporation
 * Copyright (c) 2017 Michal Sojka
 * Author: Josh Triplett <josh@joshtriplett.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <err.h>
#include <fcntl.h>
#include <linux/kvm.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define CHECK(cmd)    ({ int   __ret = (cmd); if (__ret == -1) err(1, #cmd); __ret; })
#define CHECKPTR(cmd)    ({ void *__ret = (cmd); if (!__ret) err(1, #cmd); __ret; })

int main(void)
{
    int kvm, vmfd, vcpufd, fd;
    uint8_t *mem;
    struct kvm_sregs sregs;
    size_t mmap_size;
    struct kvm_run *run;

    kvm = CHECK(open("/dev/kvm", O_RDWR | O_CLOEXEC));
    vmfd = CHECK(ioctl(kvm, KVM_CREATE_VM, (unsigned long)0));

    /* Allocate one aligned page of guest memory to hold the code. */
    mem = CHECKPTR(aligned_alloc(0x1000, 0x1000));

    /* Load the code to the guest memory */
    fd = open("hello.bin", O_RDONLY | O_CLOEXEC);
    CHECK(read(fd, mem, 0x1000));
    close(fd);

    /* Map it to the second page frame (to avoid the real-mode IDT at 0). */
    struct kvm_userspace_memory_region region = {
        .slot = 0,
        .guest_phys_addr = 0x1000,
        .memory_size = 0x1000,
        .userspace_addr = (uint64_t)mem,
    };
    CHECK(ioctl(vmfd, KVM_SET_USER_MEMORY_REGION, &region));

    vcpufd = CHECK(ioctl(vmfd, KVM_CREATE_VCPU, (unsigned long)0));

    /* Map the shared kvm_run structure and following data. */
    mmap_size = CHECK(ioctl(kvm, KVM_GET_VCPU_MMAP_SIZE, NULL));
    run = CHECKPTR(mmap(NULL, mmap_size, PROT_READ | PROT_WRITE, MAP_SHARED, vcpufd, 0));

    /* Initialize CS to point at 0, via a read-modify-write of sregs. */
    CHECK(ioctl(vcpufd, KVM_GET_SREGS, &sregs));
    sregs.cs.base = 0;
    sregs.cs.selector = 0;
    CHECK(ioctl(vcpufd, KVM_SET_SREGS, &sregs));

    /* Initialize registers: instruction pointer for our code, and
     * initial flags required by x86 architecture. */
    struct kvm_regs regs = {
        .rip = 0x1000,
        .rflags = 0x2,
    };
    CHECK(ioctl(vcpufd, KVM_SET_REGS, &regs));

    /* Repeatedly run code and handle VM exits. */
    while (1) {
        CHECK(ioctl(vcpufd, KVM_RUN, NULL));

        switch (run->exit_reason) {
        case KVM_EXIT_HLT:
            return 0;
        case KVM_EXIT_IO:
            if (run->io.direction == KVM_EXIT_IO_OUT && run->io.size == 1 && run->io.port == 0x3f8 && run->io.count == 1)
                putchar(*(((char *)run) + run->io.data_offset));
            else
                errx(1, "unhandled KVM_EXIT_IO");
            break;
        default:
            errx(1, "exit_reason = 0x%x", run->exit_reason);
        }
    }
}
