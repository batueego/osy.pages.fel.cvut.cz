#include <unistd.h>

int main()
{
   asm volatile (
      "int $0x80"
      : : "a" (4), "b" (1), "c" ("Hello World\n"), "d" (12) : "memory");
   return 0;
}
