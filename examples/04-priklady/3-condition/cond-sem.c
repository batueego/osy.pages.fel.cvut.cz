#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

struct sem {
    int cnt;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

void sem_wait(struct sem *s)
{
    pthread_mutex_lock(&s->mutex);

    while (s->cnt <= 0) {
        pthread_cond_wait(&s->cond, &s->mutex);
    }
    s->cnt--;
    pthread_mutex_unlock(&s->mutex);
}

void sem_post(struct sem *s)
{
    pthread_mutex_lock(&s->mutex);
    s->cnt++;
    pthread_cond_signal(&s->cond);
    pthread_mutex_unlock(&s->mutex);
}

void sem_init(struct sem *s, int val)
{
    s->cnt = val;
    pthread_mutex_init(&s->mutex, NULL);
    pthread_cond_init(&s->cond, NULL);
}

int buf[10];
int wr_ptr, rd_ptr;
struct sem empty, full;
pthread_mutex_t buf_mutex;

void *prod(void *n)
{
    int i;
    for (i = 0; i < 30; i++) {
        sem_wait(&empty);
        pthread_mutex_lock(&buf_mutex);
        buf[wr_ptr] = i;
        printf("Prod %i\n", buf[wr_ptr]);
        wr_ptr++;
        wr_ptr %= 10;
        pthread_mutex_unlock(&buf_mutex);
        sem_post(&full);
    }
    return NULL;
}

void *cons(void *n)
{
    int i;
    for (i = 0; i < 30; i++) {
        sem_wait(&full);
        pthread_mutex_lock(&buf_mutex);
        printf("Cons %i\n", buf[rd_ptr]);
        rd_ptr++;
        rd_ptr %= 10;
        pthread_mutex_unlock(&buf_mutex);
        sem_post(&empty);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t t_prod, t_cons;
    pthread_mutex_init(&buf_mutex, NULL);
    wr_ptr = rd_ptr = 0;
    sem_init(&empty, 10);
    sem_init(&full, 0);
    pthread_create(&t_prod, NULL, prod, NULL);
    sleep(1);
    pthread_create(&t_cons, NULL, cons, NULL);
    pthread_join(t_prod, NULL);
    pthread_join(t_cons, NULL);
    return 0;
}
